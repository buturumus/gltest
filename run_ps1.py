#!/usr/bin/python3
# run_ps1.py winrm_addr[:port]

import sys, winrm

PS1_FILE = './run.ps1'
NAME_FILE = './winrm.name'
PW_FILE = './winrm.pw'

def read_file_txt(file):
    try:
        f = open(file, 'r')
        txt = f.read().rstrip()
        f.close()
        return txt
    except:
        print('Error reading file ' + file)
        return ''

winrm_addr = sys.argv[1]
if not winrm_addr : exit

script_body = read_file_txt(PS1_FILE)
name = read_file_txt(NAME_FILE)
pw = read_file_txt(PW_FILE)
if not (script_body and name and pw) : exit

session = winrm.Session(winrm_addr, auth=(name, pw))
print(str(
    session.run_ps(script_body).std_out
).replace('\\n', '\n').replace('\\r', ''))

