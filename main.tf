# main.tf

terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }
  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
}


# resource group and network 

resource "azurerm_resource_group" "gltest_group" {
  name     = "gltestGroup"
  location = "westus3"
}

resource "azurerm_virtual_network" "gltest_net" {
  name                = "gltestNet"
  address_space       = ["10.10.0.0/16"]
  location            = azurerm_resource_group.gltest_group.location
  resource_group_name = azurerm_resource_group.gltest_group.name
}

resource "azurerm_subnet" "gltest_app_subnet" {
  name                 = "gltestAppSubnet"
  resource_group_name  = azurerm_resource_group.gltest_group.name
  virtual_network_name = azurerm_virtual_network.gltest_net.name
  address_prefixes     = ["10.10.1.0/24"]
}

resource "azurerm_public_ip" "gltest_front_ip" {
  name                         = "gltestFrontIp"
  location                     = azurerm_resource_group.gltest_group.location
  resource_group_name          = azurerm_resource_group.gltest_group.name
  allocation_method            = "Static"
  sku                          = "Standard"
}


# balancer

resource "azurerm_lb" "gltest_balancer" {
  name                = "gltestBalancer"
  location            = azurerm_resource_group.gltest_group.location
  resource_group_name = azurerm_resource_group.gltest_group.name
  sku = "Standard"

  frontend_ip_configuration {
    name                 = "gltestBalancerFrontIpConf"
    public_ip_address_id = azurerm_public_ip.gltest_front_ip.id
  }
}

# app balancer's backend pool
resource "azurerm_lb_backend_address_pool" "gltest_backend_app_pool" {
 loadbalancer_id     = azurerm_lb.gltest_balancer.id
 name                = "gltestBackendAppPool"
}
# probe for the pool
resource "azurerm_lb_probe" "gltest_app_probe" {
  loadbalancer_id = azurerm_lb.gltest_balancer.id
  name            = "gltestAppProbe"
  port            = 80
}
# balancing rule for the pool (web server role)
resource "azurerm_lb_rule" "gltest_balancer_rule" {
  loadbalancer_id                = azurerm_lb.gltest_balancer.id
  name                           = "gltestBalancerRule"
  protocol                       = "Tcp"
  frontend_port                  = 80
  backend_port                   = 80
  frontend_ip_configuration_name = "gltestBalancerFrontIpConf"
  backend_address_pool_ids       = [ azurerm_lb_backend_address_pool.gltest_backend_app_pool.id, ]
  probe_id = azurerm_lb_probe.gltest_app_probe.id 
}
# snat rules for the pool
#   to access and manage every instance with rdp
resource "azurerm_lb_nat_rule" "gltest_balancer_snat_rule_rdp" {
  count                          = 2
  resource_group_name            = azurerm_resource_group.gltest_group.name
  loadbalancer_id                = azurerm_lb.gltest_balancer.id
  name                           = "CtrlAccessRdp${count.index}"
  protocol                       = "Tcp"
  frontend_port                  = 33890 + count.index
  backend_port                   = 3389
  frontend_ip_configuration_name = "gltestBalancerFrontIpConf"
}
#   to access and manage every instance over winrm
resource "azurerm_lb_nat_rule" "gltest_balancer_snat_rule_winrm" {
  count                          = 2
  resource_group_name            = azurerm_resource_group.gltest_group.name
  loadbalancer_id                = azurerm_lb.gltest_balancer.id
  name                           = "CtrlAccessWinrm${count.index}"
  protocol                       = "Tcp"
  frontend_port                  = 59850 + count.index
  backend_port                   = 5985
  frontend_ip_configuration_name = "gltestBalancerFrontIpConf"
}


# vm

# vm's networks

resource "azurerm_network_interface" "gltest_vm_app_nic" {
  count               = 2
  name                = "gltest_vm_app_nic${count.index}"
  location            = azurerm_resource_group.gltest_group.location
  resource_group_name = azurerm_resource_group.gltest_group.name
  ip_configuration {
    name                          = "gltestVmAppNicConf${count.index}"
    subnet_id                     = azurerm_subnet.gltest_app_subnet.id
#   private_ip_address_allocation = "Dynamic"
    private_ip_address_allocation = "Static"
    private_ip_address            = "10.10.1.10${count.index}"
  }
}

# security groups

resource "azurerm_network_security_group" "gltest_vm_app_nic_sec" {
  name                = "gltestVmAppNicSec"
  location            = azurerm_resource_group.gltest_group.location
  resource_group_name = azurerm_resource_group.gltest_group.name
  # webserver
  security_rule {
    name                       = "gltestAppInRule"
    priority                   = 1003
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = 80
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  # winrm
  security_rule {
    name                       = "gltestCtrlInRuleWinrm"
    priority                   = 1002
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = 5985
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
  # rdp 
  security_rule {
    name                       = "gltestCtrlInRuleRdp"
    priority                   = 1001
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = 3389
    source_address_prefix      = "*"
    destination_address_prefix = "*"
  }
}

# bind to vm's interfaces:

# security group
resource "azurerm_network_interface_security_group_association" "gltest_vm_app_nic_sec_bind" {
  count                     = 2
  network_interface_id      = element(
    azurerm_network_interface.gltest_vm_app_nic.*.id, count.index
  ) 
  network_security_group_id = azurerm_network_security_group.gltest_vm_app_nic_sec.id
}

# backend pool
resource "azurerm_network_interface_backend_address_pool_association" "gltest_vm_app_nic_pool_bind" {
  count                   = 2
  network_interface_id    = element(
    azurerm_network_interface.gltest_vm_app_nic.*.id, count.index
  ) 
  ip_configuration_name   = "gltestVmAppNicConf${count.index}"
  backend_address_pool_id = azurerm_lb_backend_address_pool.gltest_backend_app_pool.id
}

# snat rules for mgmt
resource "azurerm_network_interface_nat_rule_association" "gltest_vm_snat_rule_rdp_bind" {
  count                   = 2
  network_interface_id    = element(
    azurerm_network_interface.gltest_vm_app_nic.*.id, count.index
  ) 
  ip_configuration_name   = "gltestVmAppNicConf${count.index}"
  nat_rule_id             = element(
    azurerm_lb_nat_rule.gltest_balancer_snat_rule_rdp.*.id, count.index
  )
}
resource "azurerm_network_interface_nat_rule_association" "gltest_vm_snat_rule_winrm_bind" {
  count                   = 2
  network_interface_id    = element(
    azurerm_network_interface.gltest_vm_app_nic.*.id, count.index
  ) 
  ip_configuration_name   = "gltestVmAppNicConf${count.index}"
  nat_rule_id             = element(
    azurerm_lb_nat_rule.gltest_balancer_snat_rule_winrm.*.id, count.index
  )
}


# other vm stuff

variable "AZ" {
  default = [ "1", "2" ]
}

resource "azurerm_windows_virtual_machine" "gltest_vm" {
  count                 = 2
  name                  = "vm${count.index}"
  location              = azurerm_resource_group.gltest_group.location
  resource_group_name   = azurerm_resource_group.gltest_group.name
  network_interface_ids = [ 
    element(azurerm_network_interface.gltest_vm_app_nic.*.id, count.index),
  ]
  size                  = "Standard_B1s"
  admin_username        = "xxx"
  admin_password        = "4ervonaKalyn@"
  zone                  = var.AZ[count.index]

  os_disk {
    name                 = "gltestVm${count.index}OsDisk"
    caching              = "ReadWrite"
    storage_account_type = "Premium_LRS"
  }

  source_image_reference {
    publisher = "MicrosoftWindowsServer"
    offer     = "WindowsServer"
    sku       = "2019-datacenter-gensecond"
    version   = "latest"
  }

}

resource "azurerm_virtual_machine_extension" "install_iis" {
  count                = 2
  name                 = "installIis${count.index}"
  virtual_machine_id   = azurerm_windows_virtual_machine.gltest_vm[count.index].id
  publisher            = "Microsoft.Compute"
  type                 = "CustomScriptExtension"
  type_handler_version = "1.10"

  # init vm string:
  #   turn IIS on
  #   turn remote powershell over http on
  #   make firewall rule for access from everywhere not subnet only 
  #   enable insecure auth, ok for test
  settings = <<SETTINGS
    {
      "commandToExecute": "powershell Add-WindowsFeature Web-Server; Add-Content -Path \"C:\\inetpub\\wwwroot\\Default.htm\" -Value $($env:computername) ; Enable-PSRemoting -Force && netsh advfirewall firewall add rule name=xxx_winrm dir=in action=allow protocol=tcp localip=any remoteip=any localport=5985 && winrm set winrm/config/service/auth @{Basic=\"true\"} && winrm set winrm/config/service @{AllowUnencrypted=\"true\"} && exit 0"
    }
    SETTINGS
}

