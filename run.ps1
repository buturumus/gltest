# run.ps1

Import-Module WebAdministration

$siteName = "lbtest"
$diskPath = "C:\inetpub\wwwroot\lbtest"
$diskParent = "C:\inetpub\wwwroot"
$indexHtmlAddr = "https://gitlab.com/buturumus/gltest/-/raw/master/Default"
$indexHtmlOnDisk = "C:\inetpub\wwwroot\lbtest\Default.htm"

# turn default site off because of same bindings
Stop-WebSite -Name 'Default Web Site'

# remove old application pool
if (Test-Path IIS:\AppPools\$siteName){
  echo "Deleting app.pool"
  Remove-WebAppPool -Name $siteName 
} else {
  echo "No app.pool to delete"
}

# remove old website
if (Get-Website -Name $siteName){
  echo "Deleting website"
  Remove-WebSite -Name $siteName
} else {
  echo "No website to delete"
}

# remove site's folder
if (Test-Path $diskPath){
  echo "Deleting site's folder"
  Remove-Item $diskPath -Recurse -Force
} else {
  echo "No folder to delete"
}

# create new site's folder
New-Item -ItemType Directory -Name $siteName -Path $diskParent
# download the only file of the project into it
Invoke-WebRequest -Uri $indexHtmlAddr -OutFile $indexHtmlOnDisk

# create new app.pool
New-WebAppPool -Name $siteName

# create new site
New-WebSite -Name $siteName -Port 80 -IPAddress * -PhysicalPath $diskPath -ApplicationPool $siteName
Set-ItemProperty "IIS:\Sites\$siteName" -Name  Bindings -value @{protocol="http";bindingInformation="*:80:*"}

